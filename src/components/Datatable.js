import {Container, Typography, Button, Grid, TableContainer, Table, TableHead, TableRow, TableCell, TableBody, Pagination } from "@mui/material";
import * as React from 'react';
import { useEffect, useState } from "react";
import ModalAddUser  from "./ModalAddUser";
import ModalEditUser from "./ModalEditUser ";
import ModalDeleteUser from "./ModalDeleteUser";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 450,
  bgcolor: "white",
  borderRadius: "12px",
  p: 4,
  alignItems: "center",
};

function Datatable() {
  const [users, setUsers] = useState([]);
  const [varRefeshPage, setVarRefeshPage] = useState(0);
  const [openModalAdd, setOpenModalAdd] = useState(false);
  const [openModalEdit, setOpenModalEdit] = useState(false);
  const [openModalDelete, setOpenModalDelete] = useState(false);



  const [rowClicked, setRowClicked] = useState([])
  const [idDelete, setIdDelete] = useState("")

  // Limit: Số lượng bản ghi trên 1 trang
  const limit = 10;

  // Số trang: Tổng số lượng sản phẩm / limit - Số lớn hơn gần nhất
  const [noPage, setNoPage] = useState(0);

  // Trang hiện tại
  const [page, setPage] = useState(1);

  const getData = async (url, Options) => {
    const response = await fetch(url, Options);
    const data = await response.json();
    return data;
  };

  const onBtnUpdateUser = (row) => {
    console.log("Nút sửa được click")
        console.log("ID: " + row.id)
        setOpenModalEdit(true)
        setRowClicked(row)

  };

  const clickDelete = (row) => {
    console.log("Nút xóa được click")
        console.log("ID: " + row.id)
        setOpenModalDelete(true)
        setIdDelete(row.id)

  };

  const onChangePagination = (event, value) => {
    setPage(value);
  };

  const onBtnInsertUser = () => {
    setOpenModalAdd(true);
  }; 
  const handleClose = () => setOpenModalAdd(false)

  const handleCloseEdit = () => setOpenModalEdit(false)

  const handleCloseDelete = () => setOpenModalDelete(false);

  useEffect(() => {
    getData("http://42.115.221.44:8080/crud-api/users/")
      .then((res) => {
        setNoPage(Math.ceil(res.length / limit));
        setUsers(res.slice((page - 1) * limit, page * limit));
        
      })
      .catch((error) => {
        console.error(error.message);
      });
  }, [varRefeshPage, page ]);

  return (
    <Container>
      <Grid align="center">
        <Typography variant="h4" gutterBottom component="div">
          Danh sách người dùng đăng ký
        </Typography>
      </Grid>
      <Grid>
        <Button variant="contained" color="success" onClick={onBtnInsertUser}>
          Thêm user
        </Button>
      </Grid>
      <Grid container>
        <Grid item>
          <TableContainer>
            <Table sx={{ minWidth: 650, maxWidth: 1050}} aria-label="simple table">
              <TableHead>
                <TableRow>
                  <TableCell align="center">ID</TableCell>
                  <TableCell align="center">Firstname</TableCell>
                  <TableCell align="center">Lastname</TableCell>
                  <TableCell align="center">Country</TableCell>
                  <TableCell align="center">Subject</TableCell>
                  <TableCell align="center">Customer Type</TableCell>
                  <TableCell align="center">Register Station</TableCell>
                  <TableCell align="center">Action</TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                {users.map((row, index) => {
                  return (
                    <TableRow key={index}>
                      <TableCell>{row.id}</TableCell>
                      <TableCell>{row.firstname}</TableCell>
                      <TableCell>{row.lastname}</TableCell>
                      <TableCell>{row.country}</TableCell>
                      <TableCell>{row.subject}</TableCell>
                      <TableCell>{row.customerType}</TableCell>
                      <TableCell>{row.registerStatus}</TableCell>
                      <TableCell align="center">
                        <Button
                          variant="contained"
                          color="primary"
                          onClick={() => {
                            onBtnUpdateUser(row);
                          }}
                        >
                          Sửa
                        </Button>
                        <Button
                          variant="contained"
                          color="error"
                          onClick={() => {
                            clickDelete(row);
                          }}
                        >
                          Xóa
                        </Button>
                      </TableCell>
                    </TableRow>
                  );
                })}
              </TableBody>
            </Table>
          </TableContainer>
        </Grid>
      </Grid>
      <Grid container mt={3} mb={2} justifyContent="flex-end">
        <Grid item>
          <Pagination
            count={noPage}
            defaultPage={page}
            onChange={onChangePagination}
          />
        </Grid>
      </Grid>
      {/* Modal */}
      <ModalAddUser varRefeshPage={varRefeshPage} setOpenModalAdd={setOpenModalAdd} openModalAdd={openModalAdd} handleClose={handleClose} style={style} getData={getData} setVarRefeshPage={setVarRefeshPage}/>
      <ModalEditUser varRefeshPage={varRefeshPage} openModalEdit={openModalEdit} handleCloseEdit={handleCloseEdit}
             style={style} getData={getData} setVarRefeshPage={setVarRefeshPage} rowClicked={rowClicked}
             />
       <ModalDeleteUser varRefeshPage={varRefeshPage} setVarRefeshPage={setVarRefeshPage} style={style} openModalDelete={openModalDelete} idDelete={idDelete} handleCloseDelete={handleCloseDelete}/>      

    </Container>
  );
}

export default Datatable;
