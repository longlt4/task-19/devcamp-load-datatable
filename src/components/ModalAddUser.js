import { Container, Typography, Button, Grid, Modal, Box, MenuItem, Select, TextField, InputLabel, FormControl, Stack, Snackbar, Alert} from "@mui/material";
import { useState, useEffect } from 'react'

function ModalAddUser({openModalAdd, setOpenModalAdd, handleClose, style, getData, setVarRefeshPage, varRefeshPage}) {

    const [firstnameForm, setFirstname] = useState("");
    const [lastnameForm, setLastname] = useState("");
    const [countryForm, setCountry] = useState("VN");
    const [subjectForm, setSubjectForm] = useState("");

    const [openAlert, setOpenAlert] = useState(false)
    const [noidungAlertValid, setNoidungAlertValid] = useState("")
    const [stutusModal, setStatusModal] = useState("error")

    const handleCloseAlert = () =>{
        setOpenAlert(false)
    }
    const onFirstnameChange = (event) =>{
        setFirstname(event.target.value)
    }
    const onLastnameChange = (event) =>{
        setLastname(event.target.value)
    }
    const onSelectCountryChange = (event) =>{
        setCountry(event.target.value)
    }

    const onSubjectChange = (event) =>{
        setSubjectForm(event.target.value)
    }

    const onBtnInsertClick = () => {
      console.log("Insert được click!")
      var vDataForm = {
          firstname: firstnameForm,
          lastname: lastnameForm,
          country: countryForm,
          subject: subjectForm
          
      }
      var vCheckData = validateDataForm(vDataForm);
      if(vCheckData){
          console.log(vDataForm)
          const body = {
              method: 'POST',
              body: JSON.stringify({
                  firstname: vDataForm.firstname,
                  lastname: vDataForm.lastname,
                  country: vDataForm.country,
                  subject: vDataForm.subject
              }),
              headers: {
                'Content-type': 'application/json; charset=UTF-8',
              }
          }
          getData('http://42.115.221.44:8080/crud-api/users/', body)
          .then((data) =>{
              setOpenAlert(true);
              setStatusModal("success")
              setNoidungAlertValid("Dữ liệu thêm thành công!")
              setOpenModalAdd(false)
              setVarRefeshPage(varRefeshPage + 1)
              setFirstname("");
              setLastname("");
              setSubjectForm("")
              console.log(data);
              //setUsers(data);
          })
          .catch((error) =>{
              setOpenAlert(true);
              setStatusModal("error")
              setNoidungAlertValid("Dữ liệu thêm thất bại!")
              console.log(error);
          })
      }
  }
  const validateDataForm = (paramDataForm) => {
      if(paramDataForm.firstname === "") {
          setOpenAlert(true);
          setStatusModal("error");
          setNoidungAlertValid("Firstname phải được điền vào!");
          return false;
      }
      if(paramDataForm.lastname === "") {
          setOpenAlert(true);
          setStatusModal("error")
          setNoidungAlertValid("Lastname phải được điền vào!");
          return false;
      }
      if(paramDataForm.subject === "") {
          setOpenAlert(true);
          setStatusModal("error")
          setNoidungAlertValid("Subject phải được điền vào!");
          return false;
      }
      else{
          return true;
      }
  }


    const onBtnCancelClick = () => {
      handleClose()
  }

    useEffect(()=> {

    }, [firstnameForm, lastnameForm, subjectForm])
  return (
    <Container>
       <Modal 
       open={openModalAdd}
       onClose={handleClose}>
        <Box sx={style}>
          <Grid container textAlign="center">
            <Grid item xs={12} lg={12} md={12} sm={12} mt={3}>
              <Typography variant="h4" gutterBottom component="div">
                Create User
              </Typography>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={2} padding={1}>
              <TextField label="First Name *" variant="outlined" fullWidth  onChange={onFirstnameChange}/>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={2} padding={1}>
              <TextField label="Last Name *" variant="outlined" fullWidth onChange={onLastnameChange} />
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={2} padding={1}>
              <TextField label="Subject *" variant="outlined" fullWidth onChange={onSubjectChange} />
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={2} padding={1}>
              <FormControl fullWidth>
                <InputLabel>Country</InputLabel>
                <Select label="Country"  fullWidth onChange={onSelectCountryChange} defaultValue="VN">
                  <MenuItem value={"VN"}>Việt Nam</MenuItem>
                  <MenuItem value={"USA"}>USA</MenuItem>
                  <MenuItem value={"AUS"}>Austrailia</MenuItem>
                  <MenuItem value={"CAN"}>Canada</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={12} lg={12} md={12} sm={12} mt={2} padding={1}>
              <Stack
                direction="row"
                spacing={2}
                sx={{ justifyContent: "center" }}
              >
                <Button
                  disableElevation
                  variant="contained"
                  color="success"
                  onClick={onBtnInsertClick}
                >
                  Insert User
                </Button>
                <Button variant="contained" color="success" onClick={onBtnCancelClick}>
                  Hủy bỏ
                </Button>
              </Stack>
            </Grid>
          </Grid>
        </Box>
      </Modal>
      <Snackbar open={openAlert} autoHideDuration={6000} onClose={handleCloseAlert}>
            <Alert onClose={handleCloseAlert} severity={stutusModal} sx={{ width: '100%' }}>
            {noidungAlertValid}
            </Alert>
      </Snackbar>
    </Container>
  );
}

export default ModalAddUser;
