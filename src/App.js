import Datatable from "./components/Datatable";
import 'bootstrap/dist/css/bootstrap.min.css';

function App() {
  return (
    <div >
      <Datatable/>
    </div>
  );
}

export default App;
